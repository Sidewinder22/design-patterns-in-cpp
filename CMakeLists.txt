# Main CMake file

cmake_minimum_required(VERSION 3.13)

project(design-patterns-in-cpp)

set(CMAKE_CXX_STANDARD 20)

SET(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/bin)

add_subdirectory(creational)

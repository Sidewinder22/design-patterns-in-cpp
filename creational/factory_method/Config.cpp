/*
 * @author	{\_Sidewinder22_/}
 * @date	25 maj 2021
 *
 * @brief    config class
 */

#include <iostream>
#include "CppApplication.hpp"
#include "JavaApplication.hpp"
#include "Config.hpp"

std::unique_ptr<Application> Config::getApp()
{
    int option = 0;

    do
    {
        std::cout << "1. Cpp, 2. Java: ";
        std::cin >> option;
    }
    while (option != 1 && option != 2);

    std::unique_ptr<Application> app;
    if (option == 1)
    {
        app = std::make_unique<CppApplication>();
    }
    else
    {
        app = std::make_unique<JavaApplication>();
    }

    return std::move(app);
}


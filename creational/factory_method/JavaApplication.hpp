/*
 * @author	{\_Sidewinder22_/}
 * @date	25 maj 2021
 *
 * @brief   Java Application class
 */

#ifndef CREATIONAL_FACTORY_METHOD_JAVAAPPLICATION_HPP_
#define CREATIONAL_FACTORY_METHOD_JAVAAPPLICATION_HPP_

#include "Application.hpp"

class JavaApplication
    : public Application
{
public:
    virtual ~JavaApplication() = default;
    std::unique_ptr<Editor> createEditor() override;
};

#endif /* CREATIONAL_FACTORY_METHOD_JAVAAPPLICATION_HPP_ */

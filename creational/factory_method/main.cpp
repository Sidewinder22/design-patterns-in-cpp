/*
 * @author	{\_Sidewinder22_/}
 * @date	24 maj 2021
 *
 * @brief   Main file
 */

#include <iostream>

#include "Editor.hpp"
#include "Application.hpp"
#include "Config.hpp"

int main()
{
    std::cout << "### Factory Method Pattern implementation ###" << std::endl;

    Config config;

    std::unique_ptr<Application> app = config.getApp();
    std::unique_ptr<Editor> editor = app->createEditor();

    editor->open();

    return 0;
}

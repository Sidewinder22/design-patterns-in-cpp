/*
 * @author	{\_Sidewinder22_/}
 * @date	25 maj 2021
 *
 * @brief   Cpp Application class
 */

#ifndef CREATIONAL_FACTORY_METHOD_CPPAPPLICATION_HPP_
#define CREATIONAL_FACTORY_METHOD_CPPAPPLICATION_HPP_

#include "Application.hpp"

class CppApplication
    : public Application
{
public:
    virtual ~CppApplication() = default;
    std::unique_ptr<Editor> createEditor() override;
};

#endif /* CREATIONAL_FACTORY_METHOD_CPPAPPLICATION_HPP_ */

/*
 * @author	{\_Sidewinder22_/}
 * @date	25 maj 2021
 *
 * @brief   Java Editor class
 */

#include <iostream>
#include "JavaEditor.hpp"
#include "JavaApplication.hpp"

std::unique_ptr<Editor> JavaApplication::createEditor()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;
    return std::move(std::make_unique<JavaEditor>());
}

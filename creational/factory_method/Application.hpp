/*
 * @author	{\_Sidewinder22_/}
 * @date	25 maj 2021
 *
 * @brief   Application base class
 */

#ifndef CREATIONAL_FACTORY_METHOD_APPLICATION_HPP_
#define CREATIONAL_FACTORY_METHOD_APPLICATION_HPP_

#include <memory>
#include "Editor.hpp"

class Application
{
public:
    virtual ~Application() = default;
    virtual std::unique_ptr<Editor> createEditor() = 0;
};

#endif /* CREATIONAL_FACTORY_METHOD_APPLICATION_HPP_ */

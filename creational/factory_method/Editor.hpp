/*
 * @author	{\_Sidewinder22_/}
 * @date	25 maj 2021
 * 
 * @brief   Editor base class
 */

#ifndef CREATIONAL_FACTORY_METHOD_EDITOR_HPP_
#define CREATIONAL_FACTORY_METHOD_EDITOR_HPP_

class Editor
{
public:
    virtual ~Editor() = default;
    virtual void open() = 0;
};

#endif /* CREATIONAL_FACTORY_METHOD_EDITOR_HPP_ */

/*
 * @author	{\_Sidewinder22_/}
 * @date	25 maj 2021
 *
 * @brief   Cpp Editor class
 */

#include <iostream>
#include "CppEditor.hpp"
#include "CppApplication.hpp"

std::unique_ptr<Editor> CppApplication::createEditor()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;
    return std::move(std::make_unique<CppEditor>());
}


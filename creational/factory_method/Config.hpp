/*
 * @author	{\_Sidewinder22_/}
 * @date	25 maj 2021
 *
 * @brief    config class
 */

#ifndef CREATIONAL_FACTORY_METHOD_CONFIG_HPP_
#define CREATIONAL_FACTORY_METHOD_CONFIG_HPP_

#include <memory>

class Config
{
public:
    std::unique_ptr<Application> getApp();
};

#endif /* CREATIONAL_FACTORY_METHOD_CONFIG_HPP_ */

/*
 * @author	{\_Sidewinder22_/}
 * @date	25 maj 2021
 * 
 * @brief   Cpp Editor class
 */

#include <iostream>
#include "CppEditor.hpp"

void CppEditor::open()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;
}

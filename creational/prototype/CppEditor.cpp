/*
 * @author	{\_Sidewinder22_/}
 * @date	25 maj 2021
 * 
 * @brief   Cpp Editor class
 */

#include <iostream>
#include "CppEditor.hpp"

    
CppEditor::CppEditor(int size)
    : size_(size)
{ }
    
void CppEditor::open()
{
    std::cout << __PRETTY_FUNCTION__ << ", size = " << size_ << std::endl;
}
    
std::unique_ptr<Editor> CppEditor::clone()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    auto newEditor = std::make_unique<CppEditor>(size_);
    return newEditor;
}

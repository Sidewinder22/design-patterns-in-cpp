/*
 * @author	{\_Sidewinder22_/}
 * @date	25 maj 2021
 *
 * @brief   Java Editor class
 */

#include <iostream>
#include "JavaEditor.hpp"

void JavaEditor::open()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;
}

std::unique_ptr<Editor> JavaEditor::clone()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    auto newEditor = std::make_unique<JavaEditor>();

    return newEditor;
}
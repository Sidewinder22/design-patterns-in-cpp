/*
 * @author	{\_Sidewinder22_/}
 * @date	25 maj 2021
 *
 * @brief   Java Editor class
 */

#ifndef CREATIONAL_FACTORY_METHOD_JAVAEDITOR_HPP_
#define CREATIONAL_FACTORY_METHOD_JAVAEDITOR_HPP_

#include "Editor.hpp"

class JavaEditor
    : public Editor
{
public:
    virtual ~JavaEditor() = default;

    void open() override;
    std::unique_ptr<Editor> clone() override;
};

#endif /* CREATIONAL_FACTORY_METHOD_JAVAEDITOR_HPP_ */

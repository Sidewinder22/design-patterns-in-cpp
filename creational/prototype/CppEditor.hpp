/*
 * @author	{\_Sidewinder22_/}
 * @date	25 maj 2021
 * 
 * @brief   Cpp Editor class
 */

#ifndef CREATIONAL_FACTORY_METHOD_CPPEDITOR_HPP_
#define CREATIONAL_FACTORY_METHOD_CPPEDITOR_HPP_

#include "Editor.hpp"

class CppEditor
    : public Editor
{
public:
    CppEditor(int size);
    virtual ~CppEditor() = default;

    void open() override;
    std::unique_ptr<Editor> clone() override;

private:
    int size_;
};

#endif /* CREATIONAL_FACTORY_METHOD_CPPEDITOR_HPP_ */

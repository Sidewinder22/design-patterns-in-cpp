/*
 * @author	{\_Sidewinder22_/}
 * @date	24 maj 2021
 *
 * @brief   Main file
 */

#include <iostream>

#include "Editor.hpp"
#include "CppEditor.hpp"
#include "JavaEditor.hpp"

int main()
{
    std::cout << "### Prototype Pattern implementation ###" << std::endl;

    std::cout << "### Cpp 1 Editor ###" << std::endl;
    auto cppEditor1 = std::make_unique<CppEditor>(201);
    cppEditor1->open();

    std::cout << "### Cpp 2 Editor ###" << std::endl;
    auto cppEditor2 = cppEditor1->clone();
    cppEditor2->open();

    std::cout << "### Java Editor ###" << std::endl;
    auto javaEditor = std::make_unique<JavaEditor>();
    javaEditor->open();

    return 0;
}

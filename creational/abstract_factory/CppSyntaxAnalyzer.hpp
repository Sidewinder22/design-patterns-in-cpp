/*
 * @author	{\_Sidewinder22_/}
 * @date	6 cze 2021
 * 
 * @brief   Cpp syntax analyzer class
 */

#ifndef CREATIONAL_ABSTRACT_FACTORY_CPPSYNTAXANALYZER_HPP_
#define CREATIONAL_ABSTRACT_FACTORY_CPPSYNTAXANALYZER_HPP_

#include "SyntaxAnalyzer.hpp"

class CppSyntaxAnalyzer
    : public SyntaxAnalyzer
{
public:
    virtual ~CppSyntaxAnalyzer() = default;

    void analyze() override;
};

#endif /* CREATIONAL_ABSTRACT_FACTORY_CPPSYNTAXANALYZER_HPP_ */

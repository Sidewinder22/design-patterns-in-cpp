/*
 * @author	{\_Sidewinder22_/}
 * @date	6 cze 2021
 * 
 * @brief   Java editor class
 */

#include <iostream>
#include "JavaEditor.hpp"

void JavaEditor::open()
{
    std::cout << "*** Java Editor ***" << std::endl;
}

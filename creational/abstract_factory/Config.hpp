/*
 * @author	{\_Sidewinder22_/}
 * @date	6 cze 2021
 * 
 * @brief   Config class
 */

#ifndef CREATIONAL_ABSTRACT_FACTORY_CONFIG_HPP_
#define CREATIONAL_ABSTRACT_FACTORY_CONFIG_HPP_

#include <memory>
#include "Factory.hpp"

class Config
{
public:
    std::unique_ptr<Factory> getFactory();
};

#endif /* CREATIONAL_ABSTRACT_FACTORY_CONFIG_HPP_ */

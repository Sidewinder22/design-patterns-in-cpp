/*
 * @author	{\_Sidewinder22_/}
 * @date	6 cze 2021
 * 
 * @brief   Factory abstract class
 */

#ifndef CREATIONAL_ABSTRACT_FACTORY_FACTORY_HPP_
#define CREATIONAL_ABSTRACT_FACTORY_FACTORY_HPP_

#include <memory>
#include "Editor.hpp"
#include "SyntaxAnalyzer.hpp"

class Factory
{
public:
    virtual ~Factory() = default;

    virtual std::unique_ptr<Editor> createEditor() = 0;
    virtual std::unique_ptr<SyntaxAnalyzer> createSyntaxAnalyzer() = 0;
};

#endif /* CREATIONAL_ABSTRACT_FACTORY_FACTORY_HPP_ */

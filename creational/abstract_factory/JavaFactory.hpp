/*
 * @author	{\_Sidewinder22_/}
 * @date	6 cze 2021
 * 
 * @brief   Java factory class
 */

#ifndef CREATIONAL_ABSTRACT_FACTORY_JAVAFACTORY_HPP_
#define CREATIONAL_ABSTRACT_FACTORY_JAVAFACTORY_HPP_

#include "Factory.hpp"

class JavaFactory
    : public Factory
{
public:
    virtual ~JavaFactory() = default;

    std::unique_ptr<Editor> createEditor() override;
    std::unique_ptr<SyntaxAnalyzer> createSyntaxAnalyzer() override;
};

#endif /* CREATIONAL_ABSTRACT_FACTORY_JAVAFACTORY_HPP_ */

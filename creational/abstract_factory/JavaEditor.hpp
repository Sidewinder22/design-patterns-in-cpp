/*
 * @author	{\_Sidewinder22_/}
 * @date	6 cze 2021
 * 
 * @brief   Java editor class
 */

#ifndef CREATIONAL_ABSTRACT_FACTORY_JAVAEDITOR_HPP_
#define CREATIONAL_ABSTRACT_FACTORY_JAVAEDITOR_HPP_

#include "Editor.hpp"

class JavaEditor
    : public Editor
{
public:
    virtual ~JavaEditor() = default;

    void open() override;
};

#endif /* CREATIONAL_ABSTRACT_FACTORY_JAVAEDITOR_HPP_ */

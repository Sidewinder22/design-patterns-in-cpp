/*
 * @author	{\_Sidewinder22_/}
 * @date	6 cze 2021
 * 
 * @brief   Syntax analyzer abstract class
 */

#ifndef CREATIONAL_ABSTRACT_FACTORY_SYNTAXANALYZER_HPP_
#define CREATIONAL_ABSTRACT_FACTORY_SYNTAXANALYZER_HPP_

class SyntaxAnalyzer
{
public:
    virtual ~SyntaxAnalyzer() = default;

    virtual void analyze() = 0;
};

#endif /* CREATIONAL_ABSTRACT_FACTORY_SYNTAXANALYZER_HPP_ */

/*
 * @author	{\_Sidewinder22_/}
 * @date	6 cze 2021
 * 
 * @brief   Editor abstract class
 */

#ifndef CREATIONAL_ABSTRACT_FACTORY_EDITOR_HPP_
#define CREATIONAL_ABSTRACT_FACTORY_EDITOR_HPP_

class Editor
{
public:
    virtual ~Editor() = default;

    virtual void open() = 0;
};

#endif /* CREATIONAL_ABSTRACT_FACTORY_EDITOR_HPP_ */

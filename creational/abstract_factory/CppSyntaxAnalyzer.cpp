/*
 * @author	{\_Sidewinder22_/}
 * @date	6 cze 2021
 * 
 * @brief   Cpp syntax analyzer class
 */

#include <iostream>
#include "CppSyntaxAnalyzer.hpp"

void CppSyntaxAnalyzer::analyze()
{
    std::cout << "*** Analyzing Cpp file ***" << std::endl;
}

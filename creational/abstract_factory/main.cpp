/*
 * @author	{\_Sidewinder22_/}
 * @date	1 cze 2021
 * 
 * @brief   Abstract factory impl main file
 */

#include <iostream>
#include "Factory.hpp"
#include "Config.hpp"

int main()
{
    std::cout << "### Abstract Factory Pattern implementation ###" << std::endl;

    Config config;

    auto factory = config.getFactory();

    auto editor = factory->createEditor();
    auto syntaxAnalyzer = factory->createSyntaxAnalyzer();

    editor->open();
    syntaxAnalyzer->analyze();

    return 0;
}

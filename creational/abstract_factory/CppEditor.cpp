/*
 * @author	{\_Sidewinder22_/}
 * @date	6 cze 2021
 * 
 * @brief   Cpp editor class
 */

#include <iostream>
#include "CppEditor.hpp"

void CppEditor::open()
{
    std::cout << "*** Cpp editor ***" << std::endl;
}

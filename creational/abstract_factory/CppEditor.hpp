/*
 * @author	{\_Sidewinder22_/}
 * @date	6 cze 2021
 * 
 * @brief   Cpp editor class
 */

#ifndef CREATIONAL_ABSTRACT_FACTORY_CPPEDITOR_HPP_
#define CREATIONAL_ABSTRACT_FACTORY_CPPEDITOR_HPP_

#include "Editor.hpp"

class CppEditor
    : public Editor
{
public:
    CppEditor() = default;
    virtual ~CppEditor() = default;

    void open() override;
};

#endif /* CREATIONAL_ABSTRACT_FACTORY_CPPEDITOR_HPP_ */

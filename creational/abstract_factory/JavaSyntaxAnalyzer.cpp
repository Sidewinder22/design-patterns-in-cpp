/*
 * @author	{\_Sidewinder22_/}
 * @date	6 cze 2021
 * 
 * @brief   Java syntax analyzer class
 */

#include <iostream>
#include "JavaSyntaxAnalyzer.hpp"


void JavaSyntaxAnalyzer::analyze()
{
    std::cout << "*** Analyzing Java file ***" << std::endl;
}

/*
 * @author	{\_Sidewinder22_/}
 * @date	6 cze 2021
 * 
 * @brief   Cpp factory class
 */

#include <iostream>
#include "CppEditor.hpp"
#include "CppSyntaxAnalyzer.hpp"
#include "CppFactory.hpp"

std::unique_ptr<Editor> CppFactory::createEditor()
{
    return std::make_unique<CppEditor>();
}

std::unique_ptr<SyntaxAnalyzer> CppFactory::createSyntaxAnalyzer()
{
    return std::make_unique<CppSyntaxAnalyzer>();
}

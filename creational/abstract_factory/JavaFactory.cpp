/*
 * @author	{\_Sidewinder22_/}
 * @date	6 cze 2021
 * 
 * @brief   Java factory class
 */

#include <iostream>
#include "JavaEditor.hpp"
#include "JavaSyntaxAnalyzer.hpp"
#include "JavaFactory.hpp"

std::unique_ptr<Editor> JavaFactory::createEditor()
{
    return std::make_unique<JavaEditor>();
}

std::unique_ptr<SyntaxAnalyzer> JavaFactory::createSyntaxAnalyzer()
{
    return std::make_unique<JavaSyntaxAnalyzer>();
}

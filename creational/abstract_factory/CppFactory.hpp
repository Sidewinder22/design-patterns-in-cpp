/*
 * @author	{\_Sidewinder22_/}
 * @date	6 cze 2021
 * 
 * @brief   Cpp factory class
 */

#ifndef CREATIONAL_ABSTRACT_FACTORY_CPPFACTORY_HPP_
#define CREATIONAL_ABSTRACT_FACTORY_CPPFACTORY_HPP_

#include "Factory.hpp"

class CppFactory
    : public Factory
{
public:
    virtual ~CppFactory() = default;

    std::unique_ptr<Editor> createEditor() override;
    std::unique_ptr<SyntaxAnalyzer> createSyntaxAnalyzer() override;
};

#endif /* CREATIONAL_ABSTRACT_FACTORY_CPPFACTORY_HPP_ */

/*
 * @author	{\_Sidewinder22_/}
 * @date	6 cze 2021
 * 
 * @brief   Config class
 */

#include <iostream>
#include "CppFactory.hpp"
#include "JavaFactory.hpp"
#include "Config.hpp"

std::unique_ptr<Factory> Config::getFactory()
{
    int option = 0;

    do
    {
        std::cout << "1. Cpp, 2. Java: ";
        std::cin >> option;
    }
    while (option != 1 && option != 2);

    std::unique_ptr<Factory> factory;
    if (option == 1)
    {
        factory = std::make_unique<CppFactory>();
    }
    else
    {
        factory = std::make_unique<JavaFactory>();
    }

    return std::move(factory);
}

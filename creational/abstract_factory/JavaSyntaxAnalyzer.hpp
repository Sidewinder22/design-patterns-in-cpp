/*
 * @author	{\_Sidewinder22_/}
 * @date	6 cze 2021
 * 
 * @brief   Java syntax analyzer class
 */

#ifndef CREATIONAL_ABSTRACT_FACTORY_JAVASYNTAXANALYZER_HPP_
#define CREATIONAL_ABSTRACT_FACTORY_JAVASYNTAXANALYZER_HPP_

#include "SyntaxAnalyzer.hpp"

class JavaSyntaxAnalyzer
    : public SyntaxAnalyzer
{
public:
    virtual ~JavaSyntaxAnalyzer() = default;

    void analyze() override;
};

#endif /* CREATIONAL_ABSTRACT_FACTORY_JAVASYNTAXANALYZER_HPP_ */

/*
 * @author	{\_Sidewinder22_/}
 * @date	9 cze 2021
 * 
 * @brief   Main file
 */

#include <iostream>
#include "Application.hpp"

int main()
{
    std::cout << "### Singleton Pattern implementation ###" << std::endl;

    auto app = Application::instance();
    app->start();

    return 0;
}

/*
 * @author	{\_Sidewinder22_/}
 * @date	8 cze 2021
 *
 * @brief   Application class
 */

#include <iostream>
#include "Application.hpp"

void Application::start()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;
}


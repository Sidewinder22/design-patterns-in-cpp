/*
 * @author	{\_Sidewinder22_/}
 * @date	8 cze 2021
 *
 * @brief   Application class
 */

#ifndef CREATIONAL_SINGLETON_APPLICATION_HPP_
#define CREATIONAL_SINGLETON_APPLICATION_HPP_

#include <memory>

class Application
{
public:
    Application(const Application& app) = delete;
    Application(Application&& app) = delete;

    Application& operator=(const Application& app) = delete;
    Application& operator=(Application&& app) = delete;

    static std::shared_ptr<Application> instance()
    {
        static std::shared_ptr<Application> instance {new Application};
        return instance;
    }

    void start();

private:
    Application() = default;
};

#endif /* CREATIONAL_SINGLETON_APPLICATION_HPP_ */

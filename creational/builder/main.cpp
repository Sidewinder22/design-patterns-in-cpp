/*
 * @author	{\_Sidewinder22_/}
 * @date	8 cze 2021
 * 
 * @brief   Main file
 */

#include <iostream>
#include <memory>
#include "Application.hpp"
#include "CppBuilder.hpp"
#include "TxtBuilder.hpp"

int main()
{
    std::cout << "### Builder Pattern implementation ###" << std::endl;

    auto app = std::make_unique<Application>();

    auto txtBuilder = std::make_shared<TxtBuilder>();
    app->makeTxt(txtBuilder);
    txtBuilder->getResults()->open();

    auto cppBuilder = std::make_shared<CppBuilder>();
    app->makeCpp(cppBuilder);
    cppBuilder->getResults()->open();

    return 0;
}

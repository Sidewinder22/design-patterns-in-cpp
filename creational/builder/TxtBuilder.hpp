/*
 * @author	{\_Sidewinder22_/}
 * @date	8 cze 2021
 * 
 * @brief   Txt builder class
 */

#ifndef CREATIONAL_BUILDER_TXTBUILDER_HPP_
#define CREATIONAL_BUILDER_TXTBUILDER_HPP_

#include <memory>
#include "Builder.hpp"
#include "TxtEditor.hpp"

class TxtBuilder
    : public Builder
{
public:
    virtual ~TxtBuilder() = default;

    void reset() override;
    void buildEditor() override;
    void buildFinder() override;
    std::shared_ptr<TxtEditor> getResults();

private:
    std::shared_ptr<TxtEditor> editor_;
};

#endif /* CREATIONAL_BUILDER_TXTBUILDER_HPP_ */

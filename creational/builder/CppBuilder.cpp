/*
 * @author	{\_Sidewinder22_/}
 * @date	8 cze 2021
 * 
 * @brief   Cpp Builder class
 */

#include "CppBuilder.hpp"

void CppBuilder::reset()
{
    if (editor_)
    {
        editor_.reset();
    }

    editor_ = std::make_shared<CppEditor>();
}

void CppBuilder::buildEditor()
{
    editor_ = std::make_shared<CppEditor>();
}

void CppBuilder::buildFinder()
{
    editor_->addFinder();
}

void CppBuilder::buildSyntaxAnalyzer()
{
    editor_->addSyntaxAnalyzer();
}

std::shared_ptr<CppEditor> CppBuilder::getResults()
{
    return editor_;
}

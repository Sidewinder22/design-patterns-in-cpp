/*
 * @author	{\_Sidewinder22_/}
 * @date	8 cze 2021
 * 
 * @brief   Application (aka Director) class
 */

#include "Application.hpp"

void Application::makeTxt(std::shared_ptr<Builder> builder)
{
    builder->buildEditor();
    builder->buildFinder();
}

void Application::makeCpp(std::shared_ptr<Builder> builder)
{
    builder->buildEditor();
    builder->buildFinder();
    builder->buildSyntaxAnalyzer();
}

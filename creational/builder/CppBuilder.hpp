/*
 * @author	{\_Sidewinder22_/}
 * @date	8 cze 2021
 * 
 * @brief   Cpp Builder class
 */

#ifndef CREATIONAL_BUILDER_CPPBUILDER_HPP_
#define CREATIONAL_BUILDER_CPPBUILDER_HPP_

#include <memory>
#include "Builder.hpp"
#include "CppEditor.hpp"

class CppBuilder
    : public Builder
{
public:
    virtual ~CppBuilder() = default;

    void reset() override;
    void buildEditor() override;
    void buildFinder() override;
    void buildSyntaxAnalyzer() override;
    std::shared_ptr<CppEditor> getResults();

private:
    std::shared_ptr<CppEditor> editor_;
};

#endif /* CREATIONAL_BUILDER_CPPBUILDER_HPP_ */

/*
 * @author	{\_Sidewinder22_/}
 * @date	6 cze 2021
 * 
 * @brief   Cpp editor class
 */

#include <iostream>
#include "CppEditor.hpp"

void CppEditor::open()
{
    std::cout << "*** Cpp editor ***" << std::endl;
}

void CppEditor::addFinder()
{
    std::cout << "*** Cpp add finder ***" << std::endl;
}

void CppEditor::addSyntaxAnalyzer()
{
    std::cout << "*** Cpp add syntax analyzer ***" << std::endl;
}

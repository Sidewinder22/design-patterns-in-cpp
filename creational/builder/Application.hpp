/*
 * @author	{\_Sidewinder22_/}
 * @date	8 cze 2021
 * 
 * @brief   Application (aka Director) class
 */

#ifndef CREATIONAL_BUILDER_APPLICATION_HPP_
#define CREATIONAL_BUILDER_APPLICATION_HPP_

#include <memory>
#include "Builder.hpp"

class Application
{
public:
    void makeTxt(std::shared_ptr<Builder> builder);
    void makeCpp(std::shared_ptr<Builder> builder);
};

#endif /* CREATIONAL_BUILDER_APPLICATION_HPP_ */

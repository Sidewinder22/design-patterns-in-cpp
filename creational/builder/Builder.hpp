/*
 * @author	{\_Sidewinder22_/}
 * @date	8 cze 2021
 * 
 * @brief   Builder interface
 */

#ifndef CREATIONAL_BUILDER_BUILDER_HPP_
#define CREATIONAL_BUILDER_BUILDER_HPP_

class Builder
{
public:
    virtual ~Builder() = default;

    virtual void reset() = 0;
    virtual void buildEditor() {};
    virtual void buildFinder() {};
    virtual void buildSyntaxAnalyzer() {};
};

#endif /* CREATIONAL_BUILDER_BUILDER_HPP_ */

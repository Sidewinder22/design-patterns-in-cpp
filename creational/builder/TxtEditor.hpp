/*
 * @author	{\_Sidewinder22_/}
 * @date	8 cze 2021
 * 
 * @brief   Txt Editor class
 */

#ifndef CREATIONAL_BUILDER_TXTEDITOR_HPP_
#define CREATIONAL_BUILDER_TXTEDITOR_HPP_

class TxtEditor
{
public:
    void open();
    void addFinder();
};

#endif /* CREATIONAL_BUILDER_TXTEDITOR_HPP_ */

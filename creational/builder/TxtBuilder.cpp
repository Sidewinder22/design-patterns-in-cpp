/*
 * @author	{\_Sidewinder22_/}
 * @date	8 cze 2021
 * 
 * @brief   Txt builder class
 */

#include "TxtBuilder.hpp"

void TxtBuilder::reset()
{
    if (editor_)
    {
        editor_.reset();
    }

    editor_ = std::make_shared<TxtEditor>();
}

void TxtBuilder::buildEditor()
{
    editor_ = std::make_shared<TxtEditor>();
}

void TxtBuilder::buildFinder()
{
    editor_->addFinder();
}

std::shared_ptr<TxtEditor> TxtBuilder::getResults()
{
    return editor_;
}


/*
 * @author	{\_Sidewinder22_/}
 * @date	6 cze 2021
 * 
 * @brief   Cpp editor class
 */

#ifndef CREATIONAL_ABSTRACT_FACTORY_CPPEDITOR_HPP_
#define CREATIONAL_ABSTRACT_FACTORY_CPPEDITOR_HPP_

class CppEditor
{
public:
    void open();
    void addFinder();
    void addSyntaxAnalyzer();
};

#endif /* CREATIONAL_ABSTRACT_FACTORY_CPPEDITOR_HPP_ */
